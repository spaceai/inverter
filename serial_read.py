from datetime import datetime
from easydict import EasyDict as edict
import json
import re
import requests
import subprocess

cmd = ['yasdishell']
input = '\n'.join(['e', '1', 'a', '1', 'p', '1', 'q']).encode('utf-8')
result = subprocess.run(cmd, stdout=subprocess.PIPE, input=input)
lines = result.stdout.split(b'\n')
# print(lines)
# lines = [line.rstrip('\n') for line in open('yasdishell.out')]

data = []
for line in lines:
    line = line.strip().decode('utf-8')
    # print(line)
    if line > '':
        data.append(line)

# print(data)

def parse(line):
    patterns = [
        r"^(\d+)\s+\|\s'\s*([\w\.\-]+)'\s\|\s'([\d\.]+)'\s*(\(\w+\))$", 
        r"^(\d+)\s+\|\s'\s*([\w\.]+)'\s\|\s'([\d\.]+)'$", 
        r"^(\d+)\s+\|\s'\s*([\w\.]+)'\s\|\s'([\w\.]+)'$", 
        r"^(\d+)\s+\|\s'\s*([\w\.\s:]+)'$"
    ]
    for expr in patterns:
        parts = re.match(expr, line)
        if parts:
            return parts.groups()
#         else:
#             print(line, expr)
    
    return ()

jsonDict = edict({"device": None, "data": [], "ts": datetime.now().strftime('%Y-%m-%d %H:%M:%S') })
for index, item in enumerate(data[:]):
    tuple = parse(data[index])
    # print(tuple)
    if len(tuple)==2:
        jsonDict.device = tuple[1]
    elif len(tuple) > 2:
        jsonDict.data.append({"channel": tuple[0], "value": tuple[2] }) # , "label": tuple[1]

jsonStr = json.dumps(jsonDict)
print('Sending', jsonStr)

# response = requests.post('http://endpoint.pieslinger.net/rock7/enertrack', json=jsonDict)
response = requests.post('http://52.2.214.60:5000/log', json=jsonDict)
print(response, response.text)
